import { LightningElement, api, track, wire } from "lwc";

export default class ValidateRequired extends LightningElement {

  handleCreate() {
    const allValid = [
      ...this.template.querySelectorAll("lightning-input")
    ].reduce((validSoFar, inputCmp) => {
      inputCmp.reportValidity();
      return validSoFar && inputCmp.checkValidity();
    }, true);
    if (allValid) {
      console.log('Success');
    }

  }